## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Rundeck. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Rundeck.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Rundeck. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">get(callback)</td>
    <td style="padding:15px">info</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configDelete(body, callback)</td>
    <td style="padding:15px">ConfigDelete</td>
    <td style="padding:15px">{base_path}/{version}/config/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configGet(key, strata, callback)</td>
    <td style="padding:15px">ConfigGet</td>
    <td style="padding:15px">{base_path}/{version}/config/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configGetCategories(callback)</td>
    <td style="padding:15px">ConfigGetCategories</td>
    <td style="padding:15px">{base_path}/{version}/config/getCategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configList(callback)</td>
    <td style="padding:15px">ConfigList</td>
    <td style="padding:15px">{base_path}/{version}/config/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">storageConfigList(callback)</td>
    <td style="padding:15px">StorageConfigList</td>
    <td style="padding:15px">{base_path}/{version}/config/listStoragePlugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configMetaList(callback)</td>
    <td style="padding:15px">ConfigMetaList</td>
    <td style="padding:15px">{base_path}/{version}/config/metaList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refresh(callback)</td>
    <td style="padding:15px">refresh</td>
    <td style="padding:15px">{base_path}/{version}/config/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restart(callback)</td>
    <td style="padding:15px">restart</td>
    <td style="padding:15px">{base_path}/{version}/config/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configSave(body, callback)</td>
    <td style="padding:15px">ConfigSave</td>
    <td style="padding:15px">{base_path}/{version}/config/save?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">storageConfigSave(body, callback)</td>
    <td style="padding:15px">StorageConfigSave</td>
    <td style="padding:15px">{base_path}/{version}/config/saveStoragePlugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiToggle(uuid, callback)</td>
    <td style="padding:15px">apiToggle</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/cluster/executions/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiToggle1(uuid, callback)</td>
    <td style="padding:15px">apiToggle_1</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/cluster/executions/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobClusterTakeoverSchedule(body, callback)</td>
    <td style="padding:15px">apiJobClusterTakeoverSchedule</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/takeover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verify(callback)</td>
    <td style="padding:15px">verify</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiStoreLicense(licenseAgreement, body, callback)</td>
    <td style="padding:15px">apiStoreLicense</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDebugEvents(project, id, callback)</td>
    <td style="padding:15px">getDebugEvents</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/project/{pathv1}/webhooks/{pathv2}/debug-events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDatasets(callback)</td>
    <td style="padding:15px">listDatasets</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/system-report/datasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataset(dataset, callback)</td>
    <td style="padding:15px">getDataset</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/system-report/datasets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionDataExport(wait, id, callback)</td>
    <td style="padding:15px">apiExecutionDataExport</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/result/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionDataAvailable(id, callback)</td>
    <td style="padding:15px">apiExecutionDataAvailable</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/result/dataAvailable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectHasHealthStatusEnhancer(project, callback)</td>
    <td style="padding:15px">apiProjectHasHealthStatusEnhancer</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/healthcheck/enhancer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRefreshHealthCheck(node, project, body, callback)</td>
    <td style="padding:15px">apiRefreshHealthCheck</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/healthcheck/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRefreshHealthCheckAll(project, callback)</td>
    <td style="padding:15px">apiRefreshHealthCheckAll</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/healthcheck/refresh/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNodeHealth(node, project, callback)</td>
    <td style="padding:15px">apiNodeHealth</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/healthcheck/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNodeHealthAll(project, includeChecks, callback)</td>
    <td style="padding:15px">apiNodeHealthAll</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/healthcheck/status/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiListAllJobsInProject(project, callback)</td>
    <td style="padding:15px">apiListAllJobsInProject</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/listAllJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectImportTour(project, tourFolder, manifestFileName, body, callback)</td>
    <td style="padding:15px">apiProjectImportTour</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/tour/import/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectList1(project, callback)</td>
    <td style="padding:15px">apiProjectList_1</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/tours?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGetProjectResource(project, pathParam, callback)</td>
    <td style="padding:15px">apiGetProjectResource</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/tours/resource/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectLoadResource(project, pathParam, body, callback)</td>
    <td style="padding:15px">apiProjectLoadResource</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/tours/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkPing(token, callback)</td>
    <td style="padding:15px">checkPing</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/checkPing/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadRunner(token, callback)</td>
    <td style="padding:15px">downloadRunner</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/download/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRunnerKey(id, pathParam, refresh, callback)</td>
    <td style="padding:15px">getRunnerKey</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/runner/{pathv1}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingRunner(id, callback)</td>
    <td style="padding:15px">pingRunner</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/runner/{pathv1}/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regenerateRunnerCreds(id, callback)</td>
    <td style="padding:15px">regenerateRunnerCreds</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/runner/{pathv1}/regenerateCreds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRunnerTags(id, callback)</td>
    <td style="padding:15px">listRunnerTags</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/runner/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runnerInfo(runnerId, callback)</td>
    <td style="padding:15px">runnerInfo</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/runner/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveRunner(runnerId, body, callback)</td>
    <td style="padding:15px">saveRunner</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/runner/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRunner(runnerId, callback)</td>
    <td style="padding:15px">deleteRunner</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/runner/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRunners(tags, localOnly, filter, status, callback)</td>
    <td style="padding:15px">listRunners</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/runners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRunner(body, callback)</td>
    <td style="padding:15px">createRunner</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/runners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProjectAssociatedTags(project, callback)</td>
    <td style="padding:15px">listProjectAssociatedTags</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ui(callback)</td>
    <td style="padding:15px">ui</td>
    <td style="padding:15px">{base_path}/{version}/runnerManagement/ui?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchTags(keyword, offset, limit, callback)</td>
    <td style="padding:15px">searchTags</td>
    <td style="padding:15px">{base_path}/{version}/runnerTag/searchTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSendPasswordReset(username, callback)</td>
    <td style="padding:15px">apiSendPasswordReset</td>
    <td style="padding:15px">{base_path}/{version}/secure/generatepasswordreset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGet(id, callback)</td>
    <td style="padding:15px">apiGet</td>
    <td style="padding:15px">{base_path}/{version}/secure/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiEdit(id, body, callback)</td>
    <td style="padding:15px">apiEdit</td>
    <td style="padding:15px">{base_path}/{version}/secure/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDelete(id, callback)</td>
    <td style="padding:15px">apiDelete</td>
    <td style="padding:15px">{base_path}/{version}/secure/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUpdateMembers(id, body, callback)</td>
    <td style="padding:15px">apiUpdateMembers</td>
    <td style="padding:15px">{base_path}/{version}/secure/role/{pathv1}/updateMembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiList(callback)</td>
    <td style="padding:15px">apiList</td>
    <td style="padding:15px">{base_path}/{version}/secure/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCreate(body, callback)</td>
    <td style="padding:15px">apiCreate</td>
    <td style="padding:15px">{base_path}/{version}/secure/roles/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGetUserRoles(id, callback)</td>
    <td style="padding:15px">apiGetUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/secure/user/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGet1(id, callback)</td>
    <td style="padding:15px">apiGet_1</td>
    <td style="padding:15px">{base_path}/{version}/secure/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiEdit1(id, callback)</td>
    <td style="padding:15px">apiEdit_1</td>
    <td style="padding:15px">{base_path}/{version}/secure/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDelete1(id, callback)</td>
    <td style="padding:15px">apiDelete_1</td>
    <td style="padding:15px">{base_path}/{version}/secure/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiList1(callback)</td>
    <td style="padding:15px">apiList_1</td>
    <td style="padding:15px">{base_path}/{version}/secure/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCreate1(callback)</td>
    <td style="padding:15px">apiCreate_1</td>
    <td style="padding:15px">{base_path}/{version}/secure/users/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemImportTour(tourFolder, manifestFileName, body, callback)</td>
    <td style="padding:15px">apiSystemImportTour</td>
    <td style="padding:15px">{base_path}/{version}/system/tour/import/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemList(callback)</td>
    <td style="padding:15px">apiSystemList</td>
    <td style="padding:15px">{base_path}/{version}/system/tours?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGetSystemResource(pathParam, callback)</td>
    <td style="padding:15px">apiGetSystemResource</td>
    <td style="padding:15px">{base_path}/{version}/system/tours/resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemLoadResource(pathParam, body, callback)</td>
    <td style="padding:15px">apiSystemLoadResource</td>
    <td style="padding:15px">{base_path}/{version}/system/tours/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRundeckEndpoints(callback)</td>
    <td style="padding:15px">apiRundeckEndpoints</td>
    <td style="padding:15px">{base_path}/{version}/tours/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserClassAllocations(callback)</td>
    <td style="padding:15px">getUserClassAllocations</td>
    <td style="padding:15px">{base_path}/{version}/userclass/allocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserClassesAvailable(callback)</td>
    <td style="padding:15px">getUserClassesAvailable</td>
    <td style="padding:15px">{base_path}/{version}/userclass/available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnabled(callback)</td>
    <td style="padding:15px">getEnabled</td>
    <td style="padding:15px">{base_path}/{version}/userclass/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserClassSelf(callback)</td>
    <td style="padding:15px">getUserClassSelf</td>
    <td style="padding:15px">{base_path}/{version}/userclass/self?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserClassAllocationsState(callback)</td>
    <td style="padding:15px">getUserClassAllocationsState</td>
    <td style="padding:15px">{base_path}/{version}/userclass/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">storeUserClasses(body, callback)</td>
    <td style="padding:15px">storeUserClasses</td>
    <td style="padding:15px">{base_path}/{version}/userclass/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserClass(username, callback)</td>
    <td style="padding:15px">getUserClass</td>
    <td style="padding:15px">{base_path}/{version}/userclass/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setUserClass(username, body, callback)</td>
    <td style="padding:15px">setUserClass</td>
    <td style="padding:15px">{base_path}/{version}/userclass/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeUserClass(username, callback)</td>
    <td style="padding:15px">removeUserClass</td>
    <td style="padding:15px">{base_path}/{version}/userclass/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoiMetricsDataAvailability(id, callback)</td>
    <td style="padding:15px">getRoiMetricsDataAvailability</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/roimetrics/available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoiMetricsDataApi(wait, id, callback)</td>
    <td style="padding:15px">getRoiMetricsDataApi</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/roimetrics/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectCalendars(project, callback)</td>
    <td style="padding:15px">apiProjectCalendars</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/calendars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiLoadProjectCalendars(project, body, callback)</td>
    <td style="padding:15px">apiLoadProjectCalendars</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/calendars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDeleteProjectCalendar(project, id, callback)</td>
    <td style="padding:15px">apiDeleteProjectCalendar</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/calendars/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemCalendars(callback)</td>
    <td style="padding:15px">apiSystemCalendars</td>
    <td style="padding:15px">{base_path}/{version}/system/calendars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiLoadSystemCalendars(body, callback)</td>
    <td style="padding:15px">apiLoadSystemCalendars</td>
    <td style="padding:15px">{base_path}/{version}/system/calendars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDeleteSystemCalendar(id, callback)</td>
    <td style="padding:15px">apiDeleteSystemCalendar</td>
    <td style="padding:15px">{base_path}/{version}/system/calendars/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeature(callback)</td>
    <td style="padding:15px">featureQueryAll</td>
    <td style="padding:15px">{base_path}/{version}/feature?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeatureFeatureName(featureName, callback)</td>
    <td style="padding:15px">featureQuery</td>
    <td style="padding:15px">{base_path}/{version}/feature/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiMetrics(name, callback)</td>
    <td style="padding:15px">apiMetrics</td>
    <td style="padding:15px">{base_path}/{version}/metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionModePassive(callback)</td>
    <td style="padding:15px">apiExecutionModePassive</td>
    <td style="padding:15px">{base_path}/{version}/system/executions/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionModeActive(callback)</td>
    <td style="padding:15px">apiExecutionModeActive</td>
    <td style="padding:15px">{base_path}/{version}/system/executions/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionModeStatus(passiveAs503, callback)</td>
    <td style="padding:15px">apiExecutionModeStatus</td>
    <td style="padding:15px">{base_path}/{version}/system/executions/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemInfo(callback)</td>
    <td style="padding:15px">apiSystemInfo</td>
    <td style="padding:15px">{base_path}/{version}/system/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiLogstorageInfo(callback)</td>
    <td style="padding:15px">apiLogstorageInfo</td>
    <td style="padding:15px">{base_path}/{version}/system/logstorage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiLogstorageListIncompleteExecutions(query, callback)</td>
    <td style="padding:15px">apiLogstorageListIncompleteExecutions</td>
    <td style="padding:15px">{base_path}/{version}/system/logstorage/incomplete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiResumeIncompleteLogstorage(callback)</td>
    <td style="padding:15px">apiResumeIncompleteLogstorage</td>
    <td style="padding:15px">{base_path}/{version}/system/logstorage/incomplete/resume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPlugins(callback)</td>
    <td style="padding:15px">listPlugins</td>
    <td style="padding:15px">{base_path}/{version}/plugin/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectToggleSCM(project, body, callback)</td>
    <td style="padding:15px">apiProjectToggleSCM</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/scm/toggle?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectActionPerform(project, integration, actionId, body, callback)</td>
    <td style="padding:15px">apiProjectActionPerform</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/scm/{pathv2}/action/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectActionInput(project, integration, actionId, callback)</td>
    <td style="padding:15px">apiProjectActionInput</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/scm/{pathv2}/action/{pathv3}/input?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectConfig(project, integration, callback)</td>
    <td style="padding:15px">apiProjectConfig</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/scm/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectDisable(project, integration, type, callback)</td>
    <td style="padding:15px">apiProjectDisable</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/scm/{pathv2}/plugin/{pathv3}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectEnable(project, integration, type, callback)</td>
    <td style="padding:15px">apiProjectEnable</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/scm/{pathv2}/plugin/{pathv3}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiPluginInput(project, integration, type, callback)</td>
    <td style="padding:15px">apiPluginInput</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/scm/{pathv2}/plugin/{pathv3}/input?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectSetup(project, integration, type, body, callback)</td>
    <td style="padding:15px">apiProjectSetup</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/scm/{pathv2}/plugin/{pathv3}/setup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiPlugins(project, integration, callback)</td>
    <td style="padding:15px">apiPlugins</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/scm/{pathv2}/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectStatus(project, integration, callback)</td>
    <td style="padding:15px">apiProjectStatus</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/scm/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSchedulerListJobsCurrentDocs(callback)</td>
    <td style="padding:15px">apiSchedulerListJobsCurrent_docs</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSchedulerListJobs(uuid, callback)</td>
    <td style="padding:15px">apiSchedulerListJobs</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/server/{pathv1}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobRun(id, argString, loglevel, asUser, filter, runAtTime, optionOPTNAME, metaKEY, body, callback)</td>
    <td style="padding:15px">apiJobRun</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobExport(id, format, callback)</td>
    <td style="padding:15px">apiJobExport</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobDelete(id, callback)</td>
    <td style="padding:15px">apiJobDelete</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFlipExecutionDisabled(id, callback)</td>
    <td style="padding:15px">apiFlipExecutionDisabled</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/execution/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFlipExecutionEnabled(id, callback)</td>
    <td style="padding:15px">apiFlipExecutionEnabled</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/execution/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobForecast(id, time, past, max, callback)</td>
    <td style="padding:15px">apiJobForecast</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/forecast?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobDetail(id, callback)</td>
    <td style="padding:15px">apiJobDetail</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobFileMultiUpload(id, body, callback)</td>
    <td style="padding:15px">apiJobFileMultiUpload</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/input/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobFileUpload(id, optionName, fileName, bodyFormData, callback)</td>
    <td style="padding:15px">apiJobFileUpload</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/input/file/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobMeta(id, meta, callback)</td>
    <td style="padding:15px">apiJobMeta</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobRetry(id, executionId, failedNodes, argString, loglevel, asUser, filter, runAtTime, optionOPTNAME, metaKEY, body, callback)</td>
    <td style="padding:15px">apiJobRetry</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/retry/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobRun1(id, argString, loglevel, asUser, filter, runAtTime, optionOPTNAME, metaKEY, body, callback)</td>
    <td style="padding:15px">apiJobRun_1</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFlipScheduleDisabled(id, callback)</td>
    <td style="padding:15px">apiFlipScheduleDisabled</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/schedule/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFlipScheduleEnabled(id, callback)</td>
    <td style="padding:15px">apiFlipScheduleEnabled</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/schedule/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobActionPerform(id, integration, actionId, body, callback)</td>
    <td style="padding:15px">apiJobActionPerform</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/scm/{pathv2}/action/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobActionInput(id, integration, actionId, callback)</td>
    <td style="padding:15px">apiJobActionInput</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/scm/{pathv2}/action/{pathv3}/input?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobDiff(id, integration, callback)</td>
    <td style="padding:15px">apiJobDiff</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/scm/{pathv2}/diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobStatus(id, integration, callback)</td>
    <td style="padding:15px">apiJobStatus</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/scm/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobWorkflow(id, callback)</td>
    <td style="padding:15px">apiJobWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobDeleteBulk(body, callback)</td>
    <td style="padding:15px">apiJobDeleteBulk</td>
    <td style="padding:15px">{base_path}/{version}/jobs/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobDeleteBulkDocs2(ids, idlist, callback)</td>
    <td style="padding:15px">apiJobDeleteBulk_docs2</td>
    <td style="padding:15px">{base_path}/{version}/jobs/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFlipExecutionDisabledBulk(ids, idlist, body, callback)</td>
    <td style="padding:15px">apiFlipExecutionDisabledBulk</td>
    <td style="padding:15px">{base_path}/{version}/jobs/execution/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFlipExecutionEnabledBulk(ids, idlist, body, callback)</td>
    <td style="padding:15px">apiFlipExecutionEnabledBulk</td>
    <td style="padding:15px">{base_path}/{version}/jobs/execution/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobFileInfo(id, callback)</td>
    <td style="padding:15px">apiJobFileInfo</td>
    <td style="padding:15px">{base_path}/{version}/jobs/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFlipScheduleDisabledBulk(ids, idlist, body, callback)</td>
    <td style="padding:15px">apiFlipScheduleDisabledBulk</td>
    <td style="padding:15px">{base_path}/{version}/jobs/schedule/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFlipScheduleEnabledBulk(ids, idlist, body, callback)</td>
    <td style="padding:15px">apiFlipScheduleEnabledBulk</td>
    <td style="padding:15px">{base_path}/{version}/jobs/schedule/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobsListv2(project, max, offset, tags, jobFilter, jobExactFilter, projFilter, groupPath, groupPathExact, descFilter, loglevelFilter, idlist, scheduledFilter, scheduleEnabledFilter, executionEnabledFilter, serverNodeUUIDFilter, daysAhead, runJobLaterFilter, paginatedRequired, callback)</td>
    <td style="padding:15px">apiJobsListv2</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobBrowseGetDocs(project, pathParam, meta, breakpoint, callback)</td>
    <td style="padding:15px">apiJobBrowseGet_docs</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/jobs/browse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobBrowse(project, pathParam, meta, breakpoint, body, callback)</td>
    <td style="padding:15px">apiJobBrowse</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/jobs/browse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobsExportv14(project, idlist, groupPath, jobFilter, format, jobExactFilter, projFilter, groupPathExact, descFilter, loglevelFilter, scheduledFilter, scheduleEnabledFilter, executionEnabledFilter, serverNodeUUIDFilter, daysAhead, runJobLaterFilter, paginatedRequired, callback)</td>
    <td style="padding:15px">apiJobsExportv14</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/jobs/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobsImportv14(project, fileformat, dupeOption, uuidOption, body, callback)</td>
    <td style="padding:15px">apiJobsImportv14</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/jobs/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWebhookDocs(project, body, callback)</td>
    <td style="padding:15px">createWebhook_docs</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/webhook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectProjectWebhookId(project, id, callback)</td>
    <td style="padding:15px">get</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/webhook/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">save(project, id, body, callback)</td>
    <td style="padding:15px">save</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/webhook/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remove(project, id, callback)</td>
    <td style="padding:15px">remove</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/webhook/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">list(project, callback)</td>
    <td style="padding:15px">list</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">post(authtoken, callback)</td>
    <td style="padding:15px">post</td>
    <td style="padding:15px">{base_path}/{version}/webhook/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiResourcev14(project, name, callback)</td>
    <td style="padding:15px">apiResourcev14</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/resource/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiResourcesv2(project, filter, callback)</td>
    <td style="padding:15px">apiResourcesv2</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSourceGet(project, index, callback)</td>
    <td style="padding:15px">apiSourceGet</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/source/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSourceGetContent(project, index, callback)</td>
    <td style="padding:15px">apiSourceGetContent</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/source/{pathv2}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSourceWriteContent(project, index, body, callback)</td>
    <td style="padding:15px">apiSourceWriteContent</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/source/{pathv2}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSourcesList(project, callback)</td>
    <td style="padding:15px">apiSourcesList</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/sources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobExecutions(id, status, max, offset, callback)</td>
    <td style="padding:15px">apiJobExecutions</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiJobExecutionsDelete(id, callback)</td>
    <td style="padding:15px">apiJobExecutionsDelete</td>
    <td style="padding:15px">{base_path}/{version}/job/{pathv1}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecution(id, callback)</td>
    <td style="padding:15px">apiExecution</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionDelete(id, callback)</td>
    <td style="padding:15px">apiExecutionDelete</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionAbort(id, asUser, forceIncomplete, callback)</td>
    <td style="padding:15px">apiExecutionAbort</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/abort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionInputFiles(id, callback)</td>
    <td style="padding:15px">apiExecutionInputFiles</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/input/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionOutput(id, nodename, stepctx, offset, lastlines, lastmod, maxlines, compacted, format, callback)</td>
    <td style="padding:15px">apiExecutionOutput</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/output?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionOutputNodeFilter(id, nodename, callback)</td>
    <td style="padding:15px">apiExecutionOutputNodeFilter</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/output/node/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionOutputNodeStepFilter(id, nodename, stepctx, callback)</td>
    <td style="padding:15px">apiExecutionOutputNodeStepFilter</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/output/node/{pathv2}/step/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionStateOutput(id, stateOnly, callback)</td>
    <td style="padding:15px">apiExecutionStateOutput</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/output/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionOutputStepFilter(id, stepctx, callback)</td>
    <td style="padding:15px">apiExecutionOutputStepFilter</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/output/step/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionState(id, callback)</td>
    <td style="padding:15px">apiExecutionState</td>
    <td style="padding:15px">{base_path}/{version}/execution/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionDeleteBulk(ids, body, callback)</td>
    <td style="padding:15px">apiExecutionDeleteBulk</td>
    <td style="padding:15px">{base_path}/{version}/executions/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionMetricsDocs(project, statusFilter, abortedbyFilter, jobIdListFilter, excludeJobIdListFilter, jobListFilter, excludeJobListFilter, groupPath, groupPathExact, excludeGroupPath, excludeGroupPathExact, jobFilter, excludeJobFilter, jobExactFilter, excludeJobExactFilter, startafterFilter, startbeforeFilter, endafterFilter, endbeforeFilter, begin, end, adhoc, recentFilter, olderFilter, userFilter, executionTypeFilter, max, offset, callback)</td>
    <td style="padding:15px">apiExecutionMetrics_docs</td>
    <td style="padding:15px">{base_path}/{version}/executions/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionsQueryv14Docs(project, statusFilter, abortedbyFilter, jobIdListFilter, excludeJobIdListFilter, jobListFilter, excludeJobListFilter, groupPath, groupPathExact, excludeGroupPath, excludeGroupPathExact, jobFilter, excludeJobFilter, jobExactFilter, excludeJobExactFilter, startafterFilter, startbeforeFilter, endafterFilter, endbeforeFilter, begin, end, adhoc, recentFilter, olderFilter, userFilter, executionTypeFilter, max, offset, callback)</td>
    <td style="padding:15px">apiExecutionsQueryv14_docs</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionMetricsProjectDocs(project, callback)</td>
    <td style="padding:15px">apiExecutionMetricsProject_docs</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/executions/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionsRunningv14(project, max, offset, jobIdFilter, includePostponed, callback)</td>
    <td style="padding:15px">apiExecutionsRunningv14</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/executions/running?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectList(callback)</td>
    <td style="padding:15px">apiProjectList</td>
    <td style="padding:15px">{base_path}/{version}/project/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectCreate(body, callback)</td>
    <td style="padding:15px">apiProjectCreate</td>
    <td style="padding:15px">{base_path}/{version}/project/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectGet(project, callback)</td>
    <td style="padding:15px">apiProjectGet</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectDelete(project, deferred, callback)</td>
    <td style="padding:15px">apiProjectDelete</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectAclsGetDocs(project, pathParam, callback)</td>
    <td style="padding:15px">apiProjectAclsGet_docs</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/acl/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectAclsPutDocs(project, pathParam, body, callback)</td>
    <td style="padding:15px">apiProjectAclsPut_docs</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/acl/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectAclsPostDocs(project, pathParam, body, callback)</td>
    <td style="padding:15px">apiProjectAclsPost_docs</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/acl/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectAclsDeleteDocs(project, pathParam, callback)</td>
    <td style="padding:15px">apiProjectAclsDelete_docs</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/acl/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectConfigGet(project, callback)</td>
    <td style="padding:15px">apiProjectConfigGet</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectConfigPut(project, body, callback)</td>
    <td style="padding:15px">apiProjectConfigPut</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectConfigKeyGet(project, keypath, callback)</td>
    <td style="padding:15px">apiProjectConfigKeyGet</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectConfigKeyPut(project, keypath, body, callback)</td>
    <td style="padding:15px">apiProjectConfigKeyPut</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectConfigKeyDelete(project, keypath, callback)</td>
    <td style="padding:15px">apiProjectConfigKeyDelete</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectExport(project, executionIds, exportAll, exportJobs, exportExecutions, exportConfigs, exportReadmes, exportAcls, exportComponentsCalendars, exportComponentsSchedule20Definitions, exportComponentsToursManager, exportComponentsNodeWizard, archiveParams, callback)</td>
    <td style="padding:15px">apiProjectExport</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectExportAsyncDocs(project, executionIds, exportAll, exportJobs, exportExecutions, exportConfigs, exportReadmes, exportAcls, exportComponentsCalendars, exportComponentsSchedule20Definitions, exportComponentsToursManager, exportComponentsNodeWizard, callback)</td>
    <td style="padding:15px">apiProjectExportAsync_docs</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/export/async?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectExportAsyncDownload(project, token, callback)</td>
    <td style="padding:15px">apiProjectExportAsyncDownload</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/export/download/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectExportAsyncStatus(project, token, callback)</td>
    <td style="padding:15px">apiProjectExportAsyncStatus</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/export/status/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectImport(project, jobUuidOption, importExecutions, importConfig, importACL, importScm, importWebhooks, whkRegenAuthTokens, importNodesSources, importComponentsNAME, importOptsNAMEKEY, body, callback)</td>
    <td style="padding:15px">apiProjectImport</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectMeta(project, meta, callback)</td>
    <td style="padding:15px">apiProjectMeta</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectFileGet(project, filename, callback)</td>
    <td style="padding:15px">apiProjectFileGet</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectFilePut(project, filename, body, callback)</td>
    <td style="padding:15px">apiProjectFilePut</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectFileDelete(project, filename, callback)</td>
    <td style="padding:15px">apiProjectFileDelete</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectDisableLater(project, body, callback)</td>
    <td style="padding:15px">apiProjectDisableLater</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/disable/later?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiProjectEnableLater(project, body, callback)</td>
    <td style="padding:15px">apiProjectEnableLater</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/enable/later?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHistoryv14(project, jobIdFilter, reportIdFilter, userFilter, statFilter, jobListFilter, excludeJobListFilter, recentFilter, begin, end, max, offset, callback)</td>
    <td style="padding:15px">apiHistoryv14</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRunCommandv14(project, filter, exec, nodeThreadcount, nodeKeepgoing, asUser, body, callback)</td>
    <td style="padding:15px">apiRunCommandv14</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/run/command?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRunScriptv14(project, filter, argString, nodeThreadcount, nodeKeepgoing, asUser, scriptInterpreter, fileExtension, interpreterArgsQuoted, body, callback)</td>
    <td style="padding:15px">apiRunScriptv14</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/run/script?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRunScriptUrlv14(project, filter, argString, scriptURL, nodeThreadcount, nodeKeepgoing, asUser, scriptInterpreter, fileExtension, interpreterArgsQuoted, body, callback)</td>
    <td style="padding:15px">apiRunScriptUrlv14</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/run/url?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGetResource(pathParam, callback)</td>
    <td style="padding:15px">apiGetResource</td>
    <td style="padding:15px">{base_path}/{version}/storage/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiPutResource(pathParam, bodyFormData, callback)</td>
    <td style="padding:15px">apiPutResource</td>
    <td style="padding:15px">{base_path}/{version}/storage/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiPostResource(pathParam, bodyFormData, callback)</td>
    <td style="padding:15px">apiPostResource</td>
    <td style="padding:15px">{base_path}/{version}/storage/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDeleteResource(pathParam, callback)</td>
    <td style="padding:15px">apiDeleteResource</td>
    <td style="padding:15px">{base_path}/{version}/storage/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemAcls(pathParam, callback)</td>
    <td style="padding:15px">apiSystemAcls</td>
    <td style="padding:15px">{base_path}/{version}/system/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemAclsPUTDocs(pathParam, body, callback)</td>
    <td style="padding:15px">apiSystemAcls_PUT_docs</td>
    <td style="padding:15px">{base_path}/{version}/system/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemAclsPOSTDocs(pathParam, body, callback)</td>
    <td style="padding:15px">apiSystemAcls_POST_docs</td>
    <td style="padding:15px">{base_path}/{version}/system/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemAclsDELETEDocs(pathParam, callback)</td>
    <td style="padding:15px">apiSystemAcls_DELETE_docs</td>
    <td style="padding:15px">{base_path}/{version}/system/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionModeLaterPassive(body, callback)</td>
    <td style="padding:15px">apiExecutionModeLaterPassive</td>
    <td style="padding:15px">{base_path}/{version}/system/executions/disable/later?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExecutionModeLaterActive(body, callback)</td>
    <td style="padding:15px">apiExecutionModeLaterActive</td>
    <td style="padding:15px">{base_path}/{version}/system/executions/enable/later?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTokenGet(tokenid, callback)</td>
    <td style="padding:15px">apiTokenGet</td>
    <td style="padding:15px">{base_path}/{version}/token/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTokenDelete(tokenid, callback)</td>
    <td style="padding:15px">apiTokenDelete</td>
    <td style="padding:15px">{base_path}/{version}/token/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTokenList(user, callback)</td>
    <td style="padding:15px">apiTokenList</td>
    <td style="padding:15px">{base_path}/{version}/tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTokenCreate(user, body, callback)</td>
    <td style="padding:15px">apiTokenCreate</td>
    <td style="padding:15px">{base_path}/{version}/tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTokenRemoveExpired(user, callback)</td>
    <td style="padding:15px">apiTokenRemoveExpired</td>
    <td style="padding:15px">{base_path}/{version}/tokens/{pathv1}/removeExpired?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserDataDocs(callback)</td>
    <td style="padding:15px">apiUserData_docs</td>
    <td style="padding:15px">{base_path}/{version}/user/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserDataPostDocs(body, callback)</td>
    <td style="padding:15px">apiUserDataPost_docs</td>
    <td style="padding:15px">{base_path}/{version}/user/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserData(username, callback)</td>
    <td style="padding:15px">apiUserData</td>
    <td style="padding:15px">{base_path}/{version}/user/info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiOtherUserDataPostDocs(username, body, callback)</td>
    <td style="padding:15px">apiOtherUserDataPost_docs</td>
    <td style="padding:15px">{base_path}/{version}/user/info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserList(callback)</td>
    <td style="padding:15px">apiUserList</td>
    <td style="padding:15px">{base_path}/{version}/user/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiListRoles(callback)</td>
    <td style="padding:15px">apiListRoles</td>
    <td style="padding:15px">{base_path}/{version}/user/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
