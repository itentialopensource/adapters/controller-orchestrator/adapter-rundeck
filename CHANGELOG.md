
## 0.2.4 [10-14-2024]

* Changes made at 2024.10.14_18:36PM

See merge request itentialopensource/adapters/adapter-rundeck!9

---

## 0.2.3 [09-16-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-rundeck!7

---

## 0.2.2 [08-14-2024]

* Changes made at 2024.08.14_17:16PM

See merge request itentialopensource/adapters/adapter-rundeck!6

---

## 0.2.1 [08-08-2024]

* Changes made at 2024.08.08_09:24AM

See merge request itentialopensource/adapters/adapter-rundeck!5

---

## 0.2.0 [07-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-rundeck!3

---

## 0.1.3 [03-28-2024]

* Changes made at 2024.03.28_13:29PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-rundeck!2

---

## 0.1.2 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/controller-orchestrator/adapter-rundeck!1

---

## 0.1.1 [03-12-2024]

* Bug fixes and performance improvements

See commit 14850d6

---
