# Rundeck

Vendor: PagerDuty
Homepage: https://www.pagerduty.com/

Product: Rundeck
Product Page: https://www.rundeck.com/

## Introduction
We classify Rundeck into the Data Center and Network Services domaina as Rundeck provides a Controller/Orchestrator solution based on Runbook Automation. 

## Why Integrate
The Rundeck adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Rundeck. With this adapter you have the ability to perform operations with Rundeck on items such as:

- Runbook Automation

## Additional Product Documentation
The [Rundeck API Reference](https://docs.rundeck.com/docs/api/)