/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-rundeck',
      type: 'Rundeck',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Rundeck = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Rundeck Adapter Test', () => {
  describe('Rundeck Class Tests', () => {
    const a = new Rundeck(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('rundeck'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('rundeck'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Rundeck', pronghornDotJson.export);
          assert.equal('Rundeck', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-rundeck', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('rundeck'));
          assert.equal('Rundeck', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-rundeck', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-rundeck', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#get - errors', () => {
      it('should have a get function', (done) => {
        try {
          assert.equal(true, typeof a.get === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configDelete - errors', () => {
      it('should have a configDelete function', (done) => {
        try {
          assert.equal(true, typeof a.configDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configGet - errors', () => {
      it('should have a configGet function', (done) => {
        try {
          assert.equal(true, typeof a.configGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.configGet(null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-configGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configGetCategories - errors', () => {
      it('should have a configGetCategories function', (done) => {
        try {
          assert.equal(true, typeof a.configGetCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configList - errors', () => {
      it('should have a configList function', (done) => {
        try {
          assert.equal(true, typeof a.configList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#storageConfigList - errors', () => {
      it('should have a storageConfigList function', (done) => {
        try {
          assert.equal(true, typeof a.storageConfigList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configMetaList - errors', () => {
      it('should have a configMetaList function', (done) => {
        try {
          assert.equal(true, typeof a.configMetaList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refresh - errors', () => {
      it('should have a refresh function', (done) => {
        try {
          assert.equal(true, typeof a.refresh === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restart - errors', () => {
      it('should have a restart function', (done) => {
        try {
          assert.equal(true, typeof a.restart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configSave - errors', () => {
      it('should have a configSave function', (done) => {
        try {
          assert.equal(true, typeof a.configSave === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.configSave(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-configSave', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#storageConfigSave - errors', () => {
      it('should have a storageConfigSave function', (done) => {
        try {
          assert.equal(true, typeof a.storageConfigSave === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.storageConfigSave(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-storageConfigSave', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiToggle - errors', () => {
      it('should have a apiToggle function', (done) => {
        try {
          assert.equal(true, typeof a.apiToggle === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.apiToggle(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiToggle', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiToggle1 - errors', () => {
      it('should have a apiToggle1 function', (done) => {
        try {
          assert.equal(true, typeof a.apiToggle1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.apiToggle1(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiToggle1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobClusterTakeoverSchedule - errors', () => {
      it('should have a apiJobClusterTakeoverSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobClusterTakeoverSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiJobClusterTakeoverSchedule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobClusterTakeoverSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verify - errors', () => {
      it('should have a verify function', (done) => {
        try {
          assert.equal(true, typeof a.verify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiStoreLicense - errors', () => {
      it('should have a apiStoreLicense function', (done) => {
        try {
          assert.equal(true, typeof a.apiStoreLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licenseAgreement', (done) => {
        try {
          a.apiStoreLicense(null, null, (data, error) => {
            try {
              const displayE = 'licenseAgreement is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiStoreLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiStoreLicense('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiStoreLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDebugEvents - errors', () => {
      it('should have a getDebugEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getDebugEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.getDebugEvents(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-getDebugEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDebugEvents('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-getDebugEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDatasets - errors', () => {
      it('should have a listDatasets function', (done) => {
        try {
          assert.equal(true, typeof a.listDatasets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataset - errors', () => {
      it('should have a getDataset function', (done) => {
        try {
          assert.equal(true, typeof a.getDataset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dataset', (done) => {
        try {
          a.getDataset(null, (data, error) => {
            try {
              const displayE = 'dataset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-getDataset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionDataExport - errors', () => {
      it('should have a apiExecutionDataExport function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionDataExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionDataExport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionDataExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionDataAvailable - errors', () => {
      it('should have a apiExecutionDataAvailable function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionDataAvailable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionDataAvailable(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionDataAvailable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectHasHealthStatusEnhancer - errors', () => {
      it('should have a apiProjectHasHealthStatusEnhancer function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectHasHealthStatusEnhancer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectHasHealthStatusEnhancer(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectHasHealthStatusEnhancer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiRefreshHealthCheck - errors', () => {
      it('should have a apiRefreshHealthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.apiRefreshHealthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiRefreshHealthCheck('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiRefreshHealthCheck', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiRefreshHealthCheckAll - errors', () => {
      it('should have a apiRefreshHealthCheckAll function', (done) => {
        try {
          assert.equal(true, typeof a.apiRefreshHealthCheckAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiRefreshHealthCheckAll(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiRefreshHealthCheckAll', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiNodeHealth - errors', () => {
      it('should have a apiNodeHealth function', (done) => {
        try {
          assert.equal(true, typeof a.apiNodeHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing node', (done) => {
        try {
          a.apiNodeHealth(null, null, (data, error) => {
            try {
              const displayE = 'node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiNodeHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiNodeHealth('fakeparam', null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiNodeHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiNodeHealthAll - errors', () => {
      it('should have a apiNodeHealthAll function', (done) => {
        try {
          assert.equal(true, typeof a.apiNodeHealthAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiNodeHealthAll(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiNodeHealthAll', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiListAllJobsInProject - errors', () => {
      it('should have a apiListAllJobsInProject function', (done) => {
        try {
          assert.equal(true, typeof a.apiListAllJobsInProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiListAllJobsInProject(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiListAllJobsInProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectImportTour - errors', () => {
      it('should have a apiProjectImportTour function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectImportTour === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectImportTour(null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectImportTour', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tourFolder', (done) => {
        try {
          a.apiProjectImportTour('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'tourFolder is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectImportTour', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing manifestFileName', (done) => {
        try {
          a.apiProjectImportTour('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'manifestFileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectImportTour', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectList1 - errors', () => {
      it('should have a apiProjectList1 function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectList1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectList1(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectList1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiGetProjectResource - errors', () => {
      it('should have a apiGetProjectResource function', (done) => {
        try {
          assert.equal(true, typeof a.apiGetProjectResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiGetProjectResource(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiGetProjectResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiGetProjectResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiGetProjectResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectLoadResource - errors', () => {
      it('should have a apiProjectLoadResource function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectLoadResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectLoadResource(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectLoadResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiProjectLoadResource('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectLoadResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkPing - errors', () => {
      it('should have a checkPing function', (done) => {
        try {
          assert.equal(true, typeof a.checkPing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing token', (done) => {
        try {
          a.checkPing(null, (data, error) => {
            try {
              const displayE = 'token is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-checkPing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadRunner - errors', () => {
      it('should have a downloadRunner function', (done) => {
        try {
          assert.equal(true, typeof a.downloadRunner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing token', (done) => {
        try {
          a.downloadRunner(null, (data, error) => {
            try {
              const displayE = 'token is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-downloadRunner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRunnerKey - errors', () => {
      it('should have a getRunnerKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRunnerKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRunnerKey(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-getRunnerKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pingRunner - errors', () => {
      it('should have a pingRunner function', (done) => {
        try {
          assert.equal(true, typeof a.pingRunner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.pingRunner(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-pingRunner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regenerateRunnerCreds - errors', () => {
      it('should have a regenerateRunnerCreds function', (done) => {
        try {
          assert.equal(true, typeof a.regenerateRunnerCreds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.regenerateRunnerCreds(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-regenerateRunnerCreds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRunnerTags - errors', () => {
      it('should have a listRunnerTags function', (done) => {
        try {
          assert.equal(true, typeof a.listRunnerTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.listRunnerTags(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-listRunnerTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runnerInfo - errors', () => {
      it('should have a runnerInfo function', (done) => {
        try {
          assert.equal(true, typeof a.runnerInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runnerId', (done) => {
        try {
          a.runnerInfo(null, (data, error) => {
            try {
              const displayE = 'runnerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-runnerInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveRunner - errors', () => {
      it('should have a saveRunner function', (done) => {
        try {
          assert.equal(true, typeof a.saveRunner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runnerId', (done) => {
        try {
          a.saveRunner(null, null, (data, error) => {
            try {
              const displayE = 'runnerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-saveRunner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRunner - errors', () => {
      it('should have a deleteRunner function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRunner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runnerId', (done) => {
        try {
          a.deleteRunner(null, (data, error) => {
            try {
              const displayE = 'runnerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-deleteRunner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRunners - errors', () => {
      it('should have a listRunners function', (done) => {
        try {
          assert.equal(true, typeof a.listRunners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRunner - errors', () => {
      it('should have a createRunner function', (done) => {
        try {
          assert.equal(true, typeof a.createRunner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRunner(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-createRunner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listProjectAssociatedTags - errors', () => {
      it('should have a listProjectAssociatedTags function', (done) => {
        try {
          assert.equal(true, typeof a.listProjectAssociatedTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.listProjectAssociatedTags(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-listProjectAssociatedTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ui - errors', () => {
      it('should have a ui function', (done) => {
        try {
          assert.equal(true, typeof a.ui === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchTags - errors', () => {
      it('should have a searchTags function', (done) => {
        try {
          assert.equal(true, typeof a.searchTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSendPasswordReset - errors', () => {
      it('should have a apiSendPasswordReset function', (done) => {
        try {
          assert.equal(true, typeof a.apiSendPasswordReset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.apiSendPasswordReset(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSendPasswordReset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiGet - errors', () => {
      it('should have a apiGet function', (done) => {
        try {
          assert.equal(true, typeof a.apiGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiEdit - errors', () => {
      it('should have a apiEdit function', (done) => {
        try {
          assert.equal(true, typeof a.apiEdit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiEdit(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiEdit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiEdit('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiEdit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiDelete - errors', () => {
      it('should have a apiDelete function', (done) => {
        try {
          assert.equal(true, typeof a.apiDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiUpdateMembers - errors', () => {
      it('should have a apiUpdateMembers function', (done) => {
        try {
          assert.equal(true, typeof a.apiUpdateMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiUpdateMembers(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiUpdateMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiUpdateMembers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiUpdateMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiList - errors', () => {
      it('should have a apiList function', (done) => {
        try {
          assert.equal(true, typeof a.apiList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiCreate - errors', () => {
      it('should have a apiCreate function', (done) => {
        try {
          assert.equal(true, typeof a.apiCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiCreate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiGetUserRoles - errors', () => {
      it('should have a apiGetUserRoles function', (done) => {
        try {
          assert.equal(true, typeof a.apiGetUserRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiGetUserRoles(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiGetUserRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiGet1 - errors', () => {
      it('should have a apiGet1 function', (done) => {
        try {
          assert.equal(true, typeof a.apiGet1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiGet1(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiGet1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiEdit1 - errors', () => {
      it('should have a apiEdit1 function', (done) => {
        try {
          assert.equal(true, typeof a.apiEdit1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiEdit1(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiEdit1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiDelete1 - errors', () => {
      it('should have a apiDelete1 function', (done) => {
        try {
          assert.equal(true, typeof a.apiDelete1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiDelete1(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiDelete1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiList1 - errors', () => {
      it('should have a apiList1 function', (done) => {
        try {
          assert.equal(true, typeof a.apiList1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiCreate1 - errors', () => {
      it('should have a apiCreate1 function', (done) => {
        try {
          assert.equal(true, typeof a.apiCreate1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSystemImportTour - errors', () => {
      it('should have a apiSystemImportTour function', (done) => {
        try {
          assert.equal(true, typeof a.apiSystemImportTour === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tourFolder', (done) => {
        try {
          a.apiSystemImportTour(null, null, null, (data, error) => {
            try {
              const displayE = 'tourFolder is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSystemImportTour', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing manifestFileName', (done) => {
        try {
          a.apiSystemImportTour('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'manifestFileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSystemImportTour', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSystemList - errors', () => {
      it('should have a apiSystemList function', (done) => {
        try {
          assert.equal(true, typeof a.apiSystemList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiGetSystemResource - errors', () => {
      it('should have a apiGetSystemResource function', (done) => {
        try {
          assert.equal(true, typeof a.apiGetSystemResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiGetSystemResource(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiGetSystemResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSystemLoadResource - errors', () => {
      it('should have a apiSystemLoadResource function', (done) => {
        try {
          assert.equal(true, typeof a.apiSystemLoadResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiSystemLoadResource(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSystemLoadResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiRundeckEndpoints - errors', () => {
      it('should have a apiRundeckEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.apiRundeckEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserClassAllocations - errors', () => {
      it('should have a getUserClassAllocations function', (done) => {
        try {
          assert.equal(true, typeof a.getUserClassAllocations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserClassesAvailable - errors', () => {
      it('should have a getUserClassesAvailable function', (done) => {
        try {
          assert.equal(true, typeof a.getUserClassesAvailable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnabled - errors', () => {
      it('should have a getEnabled function', (done) => {
        try {
          assert.equal(true, typeof a.getEnabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserClassSelf - errors', () => {
      it('should have a getUserClassSelf function', (done) => {
        try {
          assert.equal(true, typeof a.getUserClassSelf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserClassAllocationsState - errors', () => {
      it('should have a getUserClassAllocationsState function', (done) => {
        try {
          assert.equal(true, typeof a.getUserClassAllocationsState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#storeUserClasses - errors', () => {
      it('should have a storeUserClasses function', (done) => {
        try {
          assert.equal(true, typeof a.storeUserClasses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserClass - errors', () => {
      it('should have a getUserClass function', (done) => {
        try {
          assert.equal(true, typeof a.getUserClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getUserClass(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-getUserClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setUserClass - errors', () => {
      it('should have a setUserClass function', (done) => {
        try {
          assert.equal(true, typeof a.setUserClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.setUserClass(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-setUserClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeUserClass - errors', () => {
      it('should have a removeUserClass function', (done) => {
        try {
          assert.equal(true, typeof a.removeUserClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.removeUserClass(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-removeUserClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoiMetricsDataAvailability - errors', () => {
      it('should have a getRoiMetricsDataAvailability function', (done) => {
        try {
          assert.equal(true, typeof a.getRoiMetricsDataAvailability === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRoiMetricsDataAvailability(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-getRoiMetricsDataAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoiMetricsDataApi - errors', () => {
      it('should have a getRoiMetricsDataApi function', (done) => {
        try {
          assert.equal(true, typeof a.getRoiMetricsDataApi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRoiMetricsDataApi('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-getRoiMetricsDataApi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectCalendars - errors', () => {
      it('should have a apiProjectCalendars function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectCalendars === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectCalendars(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectCalendars', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiLoadProjectCalendars - errors', () => {
      it('should have a apiLoadProjectCalendars function', (done) => {
        try {
          assert.equal(true, typeof a.apiLoadProjectCalendars === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiLoadProjectCalendars(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiLoadProjectCalendars', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiDeleteProjectCalendar - errors', () => {
      it('should have a apiDeleteProjectCalendar function', (done) => {
        try {
          assert.equal(true, typeof a.apiDeleteProjectCalendar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiDeleteProjectCalendar(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiDeleteProjectCalendar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiDeleteProjectCalendar('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiDeleteProjectCalendar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSystemCalendars - errors', () => {
      it('should have a apiSystemCalendars function', (done) => {
        try {
          assert.equal(true, typeof a.apiSystemCalendars === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiLoadSystemCalendars - errors', () => {
      it('should have a apiLoadSystemCalendars function', (done) => {
        try {
          assert.equal(true, typeof a.apiLoadSystemCalendars === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiDeleteSystemCalendar - errors', () => {
      it('should have a apiDeleteSystemCalendar function', (done) => {
        try {
          assert.equal(true, typeof a.apiDeleteSystemCalendar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiDeleteSystemCalendar(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiDeleteSystemCalendar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFeature - errors', () => {
      it('should have a getFeature function', (done) => {
        try {
          assert.equal(true, typeof a.getFeature === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFeatureFeatureName - errors', () => {
      it('should have a getFeatureFeatureName function', (done) => {
        try {
          assert.equal(true, typeof a.getFeatureFeatureName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing featureName', (done) => {
        try {
          a.getFeatureFeatureName(null, (data, error) => {
            try {
              const displayE = 'featureName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-getFeatureFeatureName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiMetrics - errors', () => {
      it('should have a apiMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.apiMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.apiMetrics(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionModePassive - errors', () => {
      it('should have a apiExecutionModePassive function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionModePassive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionModeActive - errors', () => {
      it('should have a apiExecutionModeActive function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionModeActive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionModeStatus - errors', () => {
      it('should have a apiExecutionModeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionModeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSystemInfo - errors', () => {
      it('should have a apiSystemInfo function', (done) => {
        try {
          assert.equal(true, typeof a.apiSystemInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiLogstorageInfo - errors', () => {
      it('should have a apiLogstorageInfo function', (done) => {
        try {
          assert.equal(true, typeof a.apiLogstorageInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiLogstorageListIncompleteExecutions - errors', () => {
      it('should have a apiLogstorageListIncompleteExecutions function', (done) => {
        try {
          assert.equal(true, typeof a.apiLogstorageListIncompleteExecutions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiResumeIncompleteLogstorage - errors', () => {
      it('should have a apiResumeIncompleteLogstorage function', (done) => {
        try {
          assert.equal(true, typeof a.apiResumeIncompleteLogstorage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPlugins - errors', () => {
      it('should have a listPlugins function', (done) => {
        try {
          assert.equal(true, typeof a.listPlugins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectToggleSCM - errors', () => {
      it('should have a apiProjectToggleSCM function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectToggleSCM === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectToggleSCM(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectToggleSCM', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectActionPerform - errors', () => {
      it('should have a apiProjectActionPerform function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectActionPerform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectActionPerform(null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectActionPerform', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiProjectActionPerform('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectActionPerform', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionId', (done) => {
        try {
          a.apiProjectActionPerform('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'actionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectActionPerform', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiProjectActionPerform('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectActionPerform', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectActionInput - errors', () => {
      it('should have a apiProjectActionInput function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectActionInput === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectActionInput(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectActionInput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiProjectActionInput('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectActionInput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionId', (done) => {
        try {
          a.apiProjectActionInput('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'actionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectActionInput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectConfig - errors', () => {
      it('should have a apiProjectConfig function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectConfig(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiProjectConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectDisable - errors', () => {
      it('should have a apiProjectDisable function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectDisable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectDisable(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectDisable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiProjectDisable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectDisable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.apiProjectDisable('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectDisable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectEnable - errors', () => {
      it('should have a apiProjectEnable function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectEnable(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectEnable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiProjectEnable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectEnable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.apiProjectEnable('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectEnable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiPluginInput - errors', () => {
      it('should have a apiPluginInput function', (done) => {
        try {
          assert.equal(true, typeof a.apiPluginInput === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiPluginInput(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiPluginInput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiPluginInput('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiPluginInput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.apiPluginInput('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiPluginInput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectSetup - errors', () => {
      it('should have a apiProjectSetup function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectSetup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectSetup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectSetup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiProjectSetup('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectSetup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.apiProjectSetup('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectSetup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiPlugins - errors', () => {
      it('should have a apiPlugins function', (done) => {
        try {
          assert.equal(true, typeof a.apiPlugins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiPlugins(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiPlugins', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiPlugins('fakeparam', null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiPlugins', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectStatus - errors', () => {
      it('should have a apiProjectStatus function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectStatus(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiProjectStatus('fakeparam', null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSchedulerListJobsCurrentDocs - errors', () => {
      it('should have a apiSchedulerListJobsCurrentDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiSchedulerListJobsCurrentDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSchedulerListJobs - errors', () => {
      it('should have a apiSchedulerListJobs function', (done) => {
        try {
          assert.equal(true, typeof a.apiSchedulerListJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.apiSchedulerListJobs(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSchedulerListJobs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobRun - errors', () => {
      it('should have a apiJobRun function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobRun === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobRun(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobRun', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobExport - errors', () => {
      it('should have a apiJobExport function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobExport(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobDelete - errors', () => {
      it('should have a apiJobDelete function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiFlipExecutionDisabled - errors', () => {
      it('should have a apiFlipExecutionDisabled function', (done) => {
        try {
          assert.equal(true, typeof a.apiFlipExecutionDisabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiFlipExecutionDisabled(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiFlipExecutionDisabled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiFlipExecutionEnabled - errors', () => {
      it('should have a apiFlipExecutionEnabled function', (done) => {
        try {
          assert.equal(true, typeof a.apiFlipExecutionEnabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiFlipExecutionEnabled(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiFlipExecutionEnabled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobForecast - errors', () => {
      it('should have a apiJobForecast function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobForecast === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobForecast(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobForecast', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobDetail - errors', () => {
      it('should have a apiJobDetail function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobDetail(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobFileMultiUpload - errors', () => {
      it('should have a apiJobFileMultiUpload function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobFileMultiUpload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobFileMultiUpload(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobFileMultiUpload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobFileUpload - errors', () => {
      it('should have a apiJobFileUpload function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobFileUpload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobFileUpload(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobFileUpload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionName', (done) => {
        try {
          a.apiJobFileUpload('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'optionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobFileUpload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobMeta - errors', () => {
      it('should have a apiJobMeta function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobMeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobMeta(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing meta', (done) => {
        try {
          a.apiJobMeta('fakeparam', null, (data, error) => {
            try {
              const displayE = 'meta is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobRetry - errors', () => {
      it('should have a apiJobRetry function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobRetry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobRetry(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobRetry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.apiJobRetry('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobRetry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobRun1 - errors', () => {
      it('should have a apiJobRun1 function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobRun1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobRun1(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobRun1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiFlipScheduleDisabled - errors', () => {
      it('should have a apiFlipScheduleDisabled function', (done) => {
        try {
          assert.equal(true, typeof a.apiFlipScheduleDisabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiFlipScheduleDisabled(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiFlipScheduleDisabled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiFlipScheduleEnabled - errors', () => {
      it('should have a apiFlipScheduleEnabled function', (done) => {
        try {
          assert.equal(true, typeof a.apiFlipScheduleEnabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiFlipScheduleEnabled(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiFlipScheduleEnabled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobActionPerform - errors', () => {
      it('should have a apiJobActionPerform function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobActionPerform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobActionPerform(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobActionPerform', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiJobActionPerform('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobActionPerform', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionId', (done) => {
        try {
          a.apiJobActionPerform('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'actionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobActionPerform', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiJobActionPerform('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobActionPerform', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobActionInput - errors', () => {
      it('should have a apiJobActionInput function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobActionInput === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobActionInput(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobActionInput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiJobActionInput('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobActionInput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionId', (done) => {
        try {
          a.apiJobActionInput('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'actionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobActionInput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobDiff - errors', () => {
      it('should have a apiJobDiff function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobDiff(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiJobDiff('fakeparam', null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobStatus - errors', () => {
      it('should have a apiJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobStatus(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integration', (done) => {
        try {
          a.apiJobStatus('fakeparam', null, (data, error) => {
            try {
              const displayE = 'integration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobWorkflow - errors', () => {
      it('should have a apiJobWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobWorkflow(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobDeleteBulk - errors', () => {
      it('should have a apiJobDeleteBulk function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobDeleteBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobDeleteBulkDocs2 - errors', () => {
      it('should have a apiJobDeleteBulkDocs2 function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobDeleteBulkDocs2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiFlipExecutionDisabledBulk - errors', () => {
      it('should have a apiFlipExecutionDisabledBulk function', (done) => {
        try {
          assert.equal(true, typeof a.apiFlipExecutionDisabledBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiFlipExecutionEnabledBulk - errors', () => {
      it('should have a apiFlipExecutionEnabledBulk function', (done) => {
        try {
          assert.equal(true, typeof a.apiFlipExecutionEnabledBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobFileInfo - errors', () => {
      it('should have a apiJobFileInfo function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobFileInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobFileInfo(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobFileInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiFlipScheduleDisabledBulk - errors', () => {
      it('should have a apiFlipScheduleDisabledBulk function', (done) => {
        try {
          assert.equal(true, typeof a.apiFlipScheduleDisabledBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiFlipScheduleEnabledBulk - errors', () => {
      it('should have a apiFlipScheduleEnabledBulk function', (done) => {
        try {
          assert.equal(true, typeof a.apiFlipScheduleEnabledBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobsListv2 - errors', () => {
      it('should have a apiJobsListv2 function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobsListv2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiJobsListv2(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobsListv2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobBrowseGetDocs - errors', () => {
      it('should have a apiJobBrowseGetDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobBrowseGetDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiJobBrowseGetDocs(null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobBrowseGetDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiJobBrowseGetDocs('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobBrowseGetDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing meta', (done) => {
        try {
          a.apiJobBrowseGetDocs('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'meta is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobBrowseGetDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing breakpoint', (done) => {
        try {
          a.apiJobBrowseGetDocs('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'breakpoint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobBrowseGetDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobBrowse - errors', () => {
      it('should have a apiJobBrowse function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobBrowse === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiJobBrowse(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobBrowse', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiJobBrowse('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobBrowse', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing meta', (done) => {
        try {
          a.apiJobBrowse('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'meta is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobBrowse', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing breakpoint', (done) => {
        try {
          a.apiJobBrowse('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'breakpoint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobBrowse', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobsExportv14 - errors', () => {
      it('should have a apiJobsExportv14 function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobsExportv14 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiJobsExportv14(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobsExportv14', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobsImportv14 - errors', () => {
      it('should have a apiJobsImportv14 function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobsImportv14 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiJobsImportv14(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobsImportv14', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createWebhookDocs - errors', () => {
      it('should have a createWebhookDocs function', (done) => {
        try {
          assert.equal(true, typeof a.createWebhookDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.createWebhookDocs(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-createWebhookDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjectProjectWebhookId - errors', () => {
      it('should have a getProjectProjectWebhookId function', (done) => {
        try {
          assert.equal(true, typeof a.getProjectProjectWebhookId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.getProjectProjectWebhookId(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-getProjectProjectWebhookId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getProjectProjectWebhookId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-getProjectProjectWebhookId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#save - errors', () => {
      it('should have a save function', (done) => {
        try {
          assert.equal(true, typeof a.save === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.save(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-save', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.save('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-save', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remove - errors', () => {
      it('should have a remove function', (done) => {
        try {
          assert.equal(true, typeof a.remove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.remove(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-remove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.remove('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-remove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#list - errors', () => {
      it('should have a list function', (done) => {
        try {
          assert.equal(true, typeof a.list === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.list(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-list', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#post - errors', () => {
      it('should have a post function', (done) => {
        try {
          assert.equal(true, typeof a.post === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authtoken', (done) => {
        try {
          a.post(null, (data, error) => {
            try {
              const displayE = 'authtoken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-post', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiResourcev14 - errors', () => {
      it('should have a apiResourcev14 function', (done) => {
        try {
          assert.equal(true, typeof a.apiResourcev14 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiResourcev14(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiResourcev14', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.apiResourcev14('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiResourcev14', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiResourcesv2 - errors', () => {
      it('should have a apiResourcesv2 function', (done) => {
        try {
          assert.equal(true, typeof a.apiResourcesv2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiResourcesv2(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiResourcesv2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSourceGet - errors', () => {
      it('should have a apiSourceGet function', (done) => {
        try {
          assert.equal(true, typeof a.apiSourceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiSourceGet(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSourceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing index', (done) => {
        try {
          a.apiSourceGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'index is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSourceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSourceGetContent - errors', () => {
      it('should have a apiSourceGetContent function', (done) => {
        try {
          assert.equal(true, typeof a.apiSourceGetContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiSourceGetContent(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSourceGetContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing index', (done) => {
        try {
          a.apiSourceGetContent('fakeparam', null, (data, error) => {
            try {
              const displayE = 'index is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSourceGetContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSourceWriteContent - errors', () => {
      it('should have a apiSourceWriteContent function', (done) => {
        try {
          assert.equal(true, typeof a.apiSourceWriteContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiSourceWriteContent(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSourceWriteContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing index', (done) => {
        try {
          a.apiSourceWriteContent('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'index is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSourceWriteContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiSourceWriteContent('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSourceWriteContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSourcesList - errors', () => {
      it('should have a apiSourcesList function', (done) => {
        try {
          assert.equal(true, typeof a.apiSourcesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiSourcesList(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSourcesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobExecutions - errors', () => {
      it('should have a apiJobExecutions function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobExecutions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobExecutions(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobExecutions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiJobExecutionsDelete - errors', () => {
      it('should have a apiJobExecutionsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.apiJobExecutionsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiJobExecutionsDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiJobExecutionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecution - errors', () => {
      it('should have a apiExecution function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecution(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionDelete - errors', () => {
      it('should have a apiExecutionDelete function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionAbort - errors', () => {
      it('should have a apiExecutionAbort function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionAbort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionAbort(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionAbort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionInputFiles - errors', () => {
      it('should have a apiExecutionInputFiles function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionInputFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionInputFiles(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionInputFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionOutput - errors', () => {
      it('should have a apiExecutionOutput function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionOutput === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionOutput(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionOutput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionOutputNodeFilter - errors', () => {
      it('should have a apiExecutionOutputNodeFilter function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionOutputNodeFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionOutputNodeFilter(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionOutputNodeFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodename', (done) => {
        try {
          a.apiExecutionOutputNodeFilter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionOutputNodeFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionOutputNodeStepFilter - errors', () => {
      it('should have a apiExecutionOutputNodeStepFilter function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionOutputNodeStepFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionOutputNodeStepFilter(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionOutputNodeStepFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodename', (done) => {
        try {
          a.apiExecutionOutputNodeStepFilter('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionOutputNodeStepFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepctx', (done) => {
        try {
          a.apiExecutionOutputNodeStepFilter('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'stepctx is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionOutputNodeStepFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionStateOutput - errors', () => {
      it('should have a apiExecutionStateOutput function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionStateOutput === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionStateOutput(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionStateOutput', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionOutputStepFilter - errors', () => {
      it('should have a apiExecutionOutputStepFilter function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionOutputStepFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionOutputStepFilter(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionOutputStepFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepctx', (done) => {
        try {
          a.apiExecutionOutputStepFilter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'stepctx is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionOutputStepFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionState - errors', () => {
      it('should have a apiExecutionState function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.apiExecutionState(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionDeleteBulk - errors', () => {
      it('should have a apiExecutionDeleteBulk function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionDeleteBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionMetricsDocs - errors', () => {
      it('should have a apiExecutionMetricsDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionMetricsDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionsQueryv14Docs - errors', () => {
      it('should have a apiExecutionsQueryv14Docs function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionsQueryv14Docs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiExecutionsQueryv14Docs(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionsQueryv14Docs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionMetricsProjectDocs - errors', () => {
      it('should have a apiExecutionMetricsProjectDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionMetricsProjectDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiExecutionMetricsProjectDocs(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionMetricsProjectDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionsRunningv14 - errors', () => {
      it('should have a apiExecutionsRunningv14 function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionsRunningv14 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiExecutionsRunningv14(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionsRunningv14', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectList - errors', () => {
      it('should have a apiProjectList function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectCreate - errors', () => {
      it('should have a apiProjectCreate function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectGet - errors', () => {
      it('should have a apiProjectGet function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectGet(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectDelete - errors', () => {
      it('should have a apiProjectDelete function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectDelete(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectAclsGetDocs - errors', () => {
      it('should have a apiProjectAclsGetDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectAclsGetDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectAclsGetDocs(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectAclsGetDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiProjectAclsGetDocs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectAclsGetDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectAclsPutDocs - errors', () => {
      it('should have a apiProjectAclsPutDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectAclsPutDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectAclsPutDocs(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectAclsPutDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiProjectAclsPutDocs('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectAclsPutDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectAclsPostDocs - errors', () => {
      it('should have a apiProjectAclsPostDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectAclsPostDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectAclsPostDocs(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectAclsPostDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiProjectAclsPostDocs('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectAclsPostDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectAclsDeleteDocs - errors', () => {
      it('should have a apiProjectAclsDeleteDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectAclsDeleteDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectAclsDeleteDocs(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectAclsDeleteDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiProjectAclsDeleteDocs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectAclsDeleteDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectConfigGet - errors', () => {
      it('should have a apiProjectConfigGet function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectConfigGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectConfigGet(null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectConfigGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectConfigPut - errors', () => {
      it('should have a apiProjectConfigPut function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectConfigPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectConfigPut(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectConfigPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectConfigKeyGet - errors', () => {
      it('should have a apiProjectConfigKeyGet function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectConfigKeyGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectConfigKeyGet(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectConfigKeyGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keypath', (done) => {
        try {
          a.apiProjectConfigKeyGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'keypath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectConfigKeyGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectConfigKeyPut - errors', () => {
      it('should have a apiProjectConfigKeyPut function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectConfigKeyPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectConfigKeyPut(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectConfigKeyPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keypath', (done) => {
        try {
          a.apiProjectConfigKeyPut('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'keypath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectConfigKeyPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectConfigKeyDelete - errors', () => {
      it('should have a apiProjectConfigKeyDelete function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectConfigKeyDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectConfigKeyDelete(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectConfigKeyDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keypath', (done) => {
        try {
          a.apiProjectConfigKeyDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'keypath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectConfigKeyDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectExport - errors', () => {
      it('should have a apiProjectExport function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectExport(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing archiveParams', (done) => {
        try {
          a.apiProjectExport('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'archiveParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectExportAsyncDocs - errors', () => {
      it('should have a apiProjectExportAsyncDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectExportAsyncDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectExportAsyncDocs(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectExportAsyncDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectExportAsyncDownload - errors', () => {
      it('should have a apiProjectExportAsyncDownload function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectExportAsyncDownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectExportAsyncDownload(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectExportAsyncDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing token', (done) => {
        try {
          a.apiProjectExportAsyncDownload('fakeparam', null, (data, error) => {
            try {
              const displayE = 'token is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectExportAsyncDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectExportAsyncStatus - errors', () => {
      it('should have a apiProjectExportAsyncStatus function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectExportAsyncStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectExportAsyncStatus(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectExportAsyncStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing token', (done) => {
        try {
          a.apiProjectExportAsyncStatus('fakeparam', null, (data, error) => {
            try {
              const displayE = 'token is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectExportAsyncStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectImport - errors', () => {
      it('should have a apiProjectImport function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectImport(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectImport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectMeta - errors', () => {
      it('should have a apiProjectMeta function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectMeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectMeta(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing meta', (done) => {
        try {
          a.apiProjectMeta('fakeparam', null, (data, error) => {
            try {
              const displayE = 'meta is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectFileGet - errors', () => {
      it('should have a apiProjectFileGet function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectFileGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectFileGet(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectFileGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.apiProjectFileGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectFileGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectFilePut - errors', () => {
      it('should have a apiProjectFilePut function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectFilePut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectFilePut(null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectFilePut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.apiProjectFilePut('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectFilePut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectFileDelete - errors', () => {
      it('should have a apiProjectFileDelete function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectFileDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectFileDelete(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectFileDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.apiProjectFileDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectFileDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectDisableLater - errors', () => {
      it('should have a apiProjectDisableLater function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectDisableLater === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectDisableLater(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectDisableLater', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiProjectDisableLater('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectDisableLater', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiProjectEnableLater - errors', () => {
      it('should have a apiProjectEnableLater function', (done) => {
        try {
          assert.equal(true, typeof a.apiProjectEnableLater === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiProjectEnableLater(null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectEnableLater', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiProjectEnableLater('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiProjectEnableLater', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiHistoryv14 - errors', () => {
      it('should have a apiHistoryv14 function', (done) => {
        try {
          assert.equal(true, typeof a.apiHistoryv14 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiHistoryv14(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiHistoryv14', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiRunCommandv14 - errors', () => {
      it('should have a apiRunCommandv14 function', (done) => {
        try {
          assert.equal(true, typeof a.apiRunCommandv14 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiRunCommandv14(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiRunCommandv14', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiRunScriptv14 - errors', () => {
      it('should have a apiRunScriptv14 function', (done) => {
        try {
          assert.equal(true, typeof a.apiRunScriptv14 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiRunScriptv14(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiRunScriptv14', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiRunScriptUrlv14 - errors', () => {
      it('should have a apiRunScriptUrlv14 function', (done) => {
        try {
          assert.equal(true, typeof a.apiRunScriptUrlv14 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing project', (done) => {
        try {
          a.apiRunScriptUrlv14(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'project is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiRunScriptUrlv14', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiGetResource - errors', () => {
      it('should have a apiGetResource function', (done) => {
        try {
          assert.equal(true, typeof a.apiGetResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiGetResource(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiGetResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiPutResource - errors', () => {
      it('should have a apiPutResource function', (done) => {
        try {
          assert.equal(true, typeof a.apiPutResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiPutResource(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiPutResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.apiPutResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiPutResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiPostResource - errors', () => {
      it('should have a apiPostResource function', (done) => {
        try {
          assert.equal(true, typeof a.apiPostResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiPostResource(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiPostResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.apiPostResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiPostResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiDeleteResource - errors', () => {
      it('should have a apiDeleteResource function', (done) => {
        try {
          assert.equal(true, typeof a.apiDeleteResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiDeleteResource(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiDeleteResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSystemAcls - errors', () => {
      it('should have a apiSystemAcls function', (done) => {
        try {
          assert.equal(true, typeof a.apiSystemAcls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiSystemAcls(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSystemAcls', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSystemAclsPUTDocs - errors', () => {
      it('should have a apiSystemAclsPUTDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiSystemAclsPUTDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiSystemAclsPUTDocs(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSystemAclsPUTDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSystemAclsPOSTDocs - errors', () => {
      it('should have a apiSystemAclsPOSTDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiSystemAclsPOSTDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiSystemAclsPOSTDocs(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSystemAclsPOSTDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiSystemAclsDELETEDocs - errors', () => {
      it('should have a apiSystemAclsDELETEDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiSystemAclsDELETEDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.apiSystemAclsDELETEDocs(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiSystemAclsDELETEDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionModeLaterPassive - errors', () => {
      it('should have a apiExecutionModeLaterPassive function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionModeLaterPassive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiExecutionModeLaterPassive(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionModeLaterPassive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiExecutionModeLaterActive - errors', () => {
      it('should have a apiExecutionModeLaterActive function', (done) => {
        try {
          assert.equal(true, typeof a.apiExecutionModeLaterActive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiExecutionModeLaterActive(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiExecutionModeLaterActive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiTokenGet - errors', () => {
      it('should have a apiTokenGet function', (done) => {
        try {
          assert.equal(true, typeof a.apiTokenGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenid', (done) => {
        try {
          a.apiTokenGet(null, (data, error) => {
            try {
              const displayE = 'tokenid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiTokenGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiTokenDelete - errors', () => {
      it('should have a apiTokenDelete function', (done) => {
        try {
          assert.equal(true, typeof a.apiTokenDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenid', (done) => {
        try {
          a.apiTokenDelete(null, (data, error) => {
            try {
              const displayE = 'tokenid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiTokenDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiTokenList - errors', () => {
      it('should have a apiTokenList function', (done) => {
        try {
          assert.equal(true, typeof a.apiTokenList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.apiTokenList(null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiTokenList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiTokenCreate - errors', () => {
      it('should have a apiTokenCreate function', (done) => {
        try {
          assert.equal(true, typeof a.apiTokenCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.apiTokenCreate(null, null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiTokenCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.apiTokenCreate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiTokenCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiTokenRemoveExpired - errors', () => {
      it('should have a apiTokenRemoveExpired function', (done) => {
        try {
          assert.equal(true, typeof a.apiTokenRemoveExpired === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.apiTokenRemoveExpired(null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiTokenRemoveExpired', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiUserDataDocs - errors', () => {
      it('should have a apiUserDataDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiUserDataDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiUserDataPostDocs - errors', () => {
      it('should have a apiUserDataPostDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiUserDataPostDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiUserData - errors', () => {
      it('should have a apiUserData function', (done) => {
        try {
          assert.equal(true, typeof a.apiUserData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.apiUserData(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiUserData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiOtherUserDataPostDocs - errors', () => {
      it('should have a apiOtherUserDataPostDocs function', (done) => {
        try {
          assert.equal(true, typeof a.apiOtherUserDataPostDocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.apiOtherUserDataPostDocs(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-rundeck-adapter-apiOtherUserDataPostDocs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiUserList - errors', () => {
      it('should have a apiUserList function', (done) => {
        try {
          assert.equal(true, typeof a.apiUserList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiListRoles - errors', () => {
      it('should have a apiListRoles function', (done) => {
        try {
          assert.equal(true, typeof a.apiListRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
